package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(), HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("El Id del producto es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "Producto no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){

        System.out.println("addProducts");
        System.out.println("El Id del producto a crear es " + product.getId());
        System.out.println("La descripcion del producto a crear es " + product.getDesc());
        System.out.println("El precio del producto a crear es " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateProduct");
        System.out.println("El id del producto a actualizar en parametro url es " + id);
        System.out.println("El id del producto a actualizar es " + product.getId());
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(product);
        }

        return new ResponseEntity<>(
                product
                , productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado"
                , deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
