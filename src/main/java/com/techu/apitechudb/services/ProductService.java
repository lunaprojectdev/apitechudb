package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product){
        System.out.println("add ProductService");

        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product){
        System.out.println("update ProductService");

        return this.productRepository.save(product);
    }

    public boolean delete(String id){
        System.out.println("delete ProductService");
        boolean result = false;

        if(this.findById(id).isPresent()){
            System.out.println("Producto encontrado, borrando");
            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
